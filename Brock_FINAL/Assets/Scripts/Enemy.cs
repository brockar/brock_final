﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    public GameObject target;
    public Transform targetPoint;
    public Transform movePoint;
    public Animator animator;
    public SpriteRenderer enemyRender;
    public LayerMask colliders;
    public LayerMask actors;

    Vector3 hVectorPos = new Vector3(1f, 0f, 0f);
    Vector3 vVectorPos = new Vector3(0f, 1f, 0f);
    Vector3 hVectorNeg = new Vector3(-1f, 0f, 0f);
    Vector3 vVectorNeg = new Vector3(0f, -1f, 0f);

    // Start is called before the first frame update
    void Start()
    {
        attack = 1f;
        movePoint.parent = null;
        targetPoint = target.transform;
        isPlayer = false;
        InvokeRepeating("ResetAttacked", 2f, 2f);
    }

    public void Collide(GameObject collided, Vector3 direction)
    {
        if (collided.CompareTag("Item"))
        {
            movePoint.position += direction;
        }
        else if (collided.CompareTag("Stairs"))
        {
            movePoint.position += direction;
        }
        else
        {
            //we are at a wall and can't move

            return;
        }
    }

    public void Attack(Player target, float damage)
    {
        target.TakeDamage(damage);
        attacked = true;
    }

    /* Action function, invoked every 2 seconds from GameController.
     * Enemy moves towards Player if Player is within aggro range
     * (to prevent wall kiting). Enemy may hit Player if close enough.
     */
    public void Act()
    {
        if (Vector3.Distance(transform.position, movePoint.position) <= .05f)
        {
            float targX = targetPoint.position.x;
            float targY = targetPoint.position.y;

            float currX = transform.position.x;
            float currY = transform.position.y;

            float distX = Mathf.Abs(currX - targX);
            float distY = Mathf.Abs(currY - targY);

            if (distX >= distY && distX >= 1 && distX <= 5)
            {
                Collider2D collidedCPos = Physics2D.OverlapCircle(movePoint.position + hVectorPos, .2f, colliders);
                Collider2D collidedAPos = Physics2D.OverlapCircle(movePoint.position + hVectorPos, .2f, actors);
                Collider2D collidedCNeg = Physics2D.OverlapCircle(movePoint.position + hVectorNeg, .2f, colliders);
                Collider2D collidedANeg = Physics2D.OverlapCircle(movePoint.position + hVectorNeg, .2f, actors);
                if (currX < targX) //move right
                {
                    enemyRender.flipX = true;
                    animator.SetBool("IsWalking", true);
                    if (collidedCPos)
                    {
                        if (collidedCPos.gameObject)
                        {
                            Collide(collidedCPos.gameObject, hVectorPos);
                        }
                    }
                    else if (collidedAPos)
                    {
                        if (target.GetComponent<Player>()) //PLAYER!!!!
                        {
                            Attack(target.GetComponent<Player>(), attack);
                        }
                        else //about to collide with another enemy
                        {
                            Collide(collidedCPos.gameObject, hVectorPos);
                        }
                    }
                    else
                    {
                        movePoint.position += hVectorPos;
                    }
                }
                else //move left
                {
                    enemyRender.flipX = true;
                    animator.SetBool("IsWalking", true);
                    if (collidedCNeg)
                    {
                        if (collidedCNeg.gameObject)
                        {
                            Collide(collidedCNeg.gameObject, hVectorNeg);
                        }
                    }
                    else if (collidedANeg)
                    {
                        if (target.GetComponent<Player>()) //PLAYER!!!!
                        {
                            Attack(target.GetComponent<Player>(), attack);
                        }
                        else //about to collide with another enemy
                        {
                            Collide(collidedCNeg.gameObject, hVectorNeg);
                        }
                    }
                    else
                    {
                        movePoint.position += hVectorNeg;
                    }
                }
                //canAct = false;
                animator.SetBool("IsWalking", false);
            }
            else if (distY > distX && distY >= 1 && distY <= 5)
            {
                Collider2D collidedCPos = Physics2D.OverlapCircle(movePoint.position + vVectorPos, .2f, colliders);
                Collider2D collidedAPos = Physics2D.OverlapCircle(movePoint.position + vVectorPos, .2f, actors);
                Collider2D collidedCNeg = Physics2D.OverlapCircle(movePoint.position + vVectorNeg, .2f, colliders);
                Collider2D collidedANeg = Physics2D.OverlapCircle(movePoint.position + vVectorNeg, .2f, actors);
                if (currY < targY) //move right
                {
                    enemyRender.flipX = true;
                    animator.SetBool("IsWalking", true);
                    if (collidedCPos)
                    {
                        if (collidedCPos.gameObject)
                        {
                            Collide(collidedCPos.gameObject, vVectorPos);
                        }
                    }
                    else if (collidedAPos)
                    {
                        if (target.GetComponent<Player>()) //PLAYER!!!!
                        {
                            Attack(target.GetComponent<Player>(), attack);
                        }
                        else //about to collide with another enemy
                        {
                            Collide(collidedCPos.gameObject, vVectorPos);
                        }
                    }
                    else
                    {
                        movePoint.position += vVectorPos;
                    }
                }
                else //move left
                {
                    enemyRender.flipX = true;
                    animator.SetBool("IsWalking", true);
                    if (collidedCNeg)
                    {
                        if (collidedCNeg.gameObject)
                        {
                            Collide(collidedCNeg.gameObject, vVectorNeg);
                        }
                    }
                    else if (collidedANeg)
                    {
                        if (target.GetComponent<Player>()) //PLAYER!!!!
                        {
                            Attack(target.GetComponent<Player>(), attack);
                        }
                        else //about to collide with another enemy
                        {
                            Collide(collidedCNeg.gameObject, vVectorNeg);
                        }
                    }
                    else
                    {
                        movePoint.position += vVectorNeg;
                    }
                }
                animator.SetBool("IsWalking", false);
            }
        }
        //this is where we move for real
        if (!attacked)
        {
            canMove = true;
        }
    }

    private void Update()
    {
        if (canMove)
        {
            if (Vector3.Distance(transform.position, movePoint.position) <= .05f)
            {
                canMove = false;
            }
            else
            {
                transform.position = Vector3.MoveTowards(transform.position, movePoint.position, speed * Time.deltaTime);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public List<Character> characters; //will fill up with characters
    public Character current;
    public int curTurn;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Character chara in characters)
        {
            if (!chara.isPlayer)
            {
                chara.InvokeRepeating("Act", 2f, 2f);
            }
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}

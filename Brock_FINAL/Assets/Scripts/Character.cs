﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public abstract class Character : MonoBehaviour
{
    public float speed = 3f;
    public float healthPoints;
    public float attack;
    public bool attacked = false;
    public bool isPlayer;
    public bool canMove = false;
    public GameObject gameOver;

    public Character()
    {
        //do we need a constructor? put it here i guess
    }

    public virtual void TakeTurn()
    {
        //takes turn in queue
    }

    public void TakeDamage(float damage)
    {
        healthPoints -= damage;
        if (healthPoints <= 0)
        {
            if (isPlayer)
            {
                gameOver.SetActive(true);
                Invoke("StartOver", 5f);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
    
    private void StartOver()
    {
        SceneManager.LoadScene("Level1");
    }

    private void ResetAttacked()
    {
        attacked = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public bool gotGem;
    public GameObject player;
    public GameObject door;
    public GameObject winScreen;

    // Start is called before the first frame update
    void Start()
    {
        gotGem = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gotGem)
        {
            if (collision.gameObject == player)
            {
                //end game, they win!
                Time.timeScale = 0;
                winScreen.SetActive(true);
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (gotGem)
        {
            door.layer = 0;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public Transform movePoint;
    public Animator animator;
    public SpriteRenderer playerRender;
    public LayerMask colliders;
    public LayerMask actors;
    public GameObject keyGem;
    public DoorScript barrierLock;

    // Start is called before the first frame update
    void Start()
    {
        attack = 1f;
        healthPoints = 3f;
        movePoint.parent = null;
        isPlayer = true;
        InvokeRepeating("ResetAttacked", 2f, 2f);
    }

    public void Collide(GameObject collided, Vector3 direction)
    {
        if (collided.CompareTag("Item"))
        {
            PickUp(collided);
            movePoint.position += direction;
        }
        else
        {
            return;
        }
    }

    public void Attack(Enemy target, float damage)
    {
        if (!attacked)
        {
            target.TakeDamage(damage);
            attacked = true;
        }
    }

    public void PickUp(GameObject item)
    {
        //if the item is the inputted gem, door is unlocked (3rd level only)
        if (item == keyGem)
        {
            barrierLock.gotGem = true;
            Destroy(item);
        }
        //else, add HP if needed and destroy item
        else if (healthPoints < 3)
        {
            healthPoints++;
            Destroy(item);
        }
    }


    private void Update()
    {
        if (Vector3.Distance(transform.position, movePoint.position) <= .05f)
        {
            float hInput = Input.GetAxisRaw("Horizontal");
            float vInput = Input.GetAxisRaw("Vertical");
            Vector3 hVector = new Vector3(hInput, 0f, 0f);
            Vector3 vVector = new Vector3(0f, vInput, 0f);

            //this is if / else if so that they can ONLY move vertically/horizontally
            if (Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 1f)
            {
                Collider2D collidedC = Physics2D.OverlapCircle(movePoint.position + hVector, .2f, colliders);
                Collider2D collidedA = Physics2D.OverlapCircle(movePoint.position + hVector, .2f, actors);
                if (collidedC) //"are we about to collide into something?"
                {
                    if (collidedC.gameObject)
                    {
                        Collide(collidedC.gameObject, hVector);
                    }
                }
                else if (collidedA)
                {
                    Attack(collidedA.gameObject.GetComponent<Enemy>(), attack);
                }
                else //"nah we're good :)"
                {
                    movePoint.position += hVector;
                }
            }
            else if (Mathf.Abs(Input.GetAxisRaw("Vertical")) == 1f)
            {
                Collider2D collidedC = Physics2D.OverlapCircle(movePoint.position + vVector, .2f, colliders);
                Collider2D collidedA = Physics2D.OverlapCircle(movePoint.position + vVector, .2f, actors);
                if (collidedC) //"are we about to collide into something?"
                {
                    if (collidedC.gameObject)
                    {
                        Collide(collidedC.gameObject, vVector);
                    }
                    //otherwise he just won't move
                }
                else if (collidedA)
                {
                    Attack(collidedA.gameObject.GetComponent<Enemy>(), attack);
                    attacked = false;
                }
                else //"nah we're good :)"
                {
                    movePoint.position += vVector;
                }
            }

            if (hInput > 0f)
            {
                playerRender.flipX = false;
            }
            else if (hInput < 0f) playerRender.flipX = true;

            if ((Mathf.Abs(hInput) == 1f) | (Mathf.Abs(vInput) == 1f))
            {
                animator.SetBool("IsWalking", true);
            }
            else animator.SetBool("IsWalking", false);
        }
        transform.position = Vector3.MoveTowards(transform.position, movePoint.position, speed * Time.deltaTime);
    }
}

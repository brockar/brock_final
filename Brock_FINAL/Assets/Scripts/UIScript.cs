﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    public Player player;
    public Text healthText;
    // Start is called before the first frame update
    void Start()
    {
        healthText.text = "HP: " + player.healthPoints + "/3";
    }

    // Update is called once per frame
    void Update()
    {
        healthText.text = "HP: " + player.healthPoints + "/3";
    }
}
